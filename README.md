# Shopping-list

Simple repository to practise git basics on a shopping list used in the context of [this serie of videos](https://www.youtube.com/playlist?list=PLqmwiCS_fyTUVFmfMCOTxGBRP7cRiPQAK). If your are following this course fully online, it is a good idea to reproduce what is done in the videos. The first exercices of the hands-on are here as a recap of the first videos.

the empty line

The objective of this course is to give the basic usage of git:
* understand the status of your local repository (status, log, diff)
* create commit (add, commit)
* create and merge branches (branch, merge)
* retrieve the status of your project or a file at any point in its history (checkout)
* interact with a remote repository (fetch, push pull)
* understand what a conflict is and solve to be able to solve it
* have notions on the internal mechanisms of git
* have notions on how the gitlab interface works

the second empty line


the third empty line

To get started with the hands-on
1. Fork the project on the web interface as explained in [this video](https://www.youtube.com/watch?v=ACDzEX0f1Gw&list=PLqmwiCS_fyTUVFmfMCOTxGBRP7cRiPQAK&index=2)
2. Clone the forked project on your local machine
```bash
git clone https://git.univ-pau.fr/YOUR_SPACE/shopping-list.git
```

Daily life git usage
--------------------
1. Check regularly the status and the history of your project in the local repository with `git status` and `git log`
2. Make a change in the `list.txt` file with your favourite text editor and check status and history
3. Add `list.txt` to the staging area with `git add list.txt` and check status and history
3. Create a commit with `git commit` and check status and history
4. Push your new revision on the remote repository with `git push origin master` and check status and history locally and on the remote repository

Repeat those steps several times to be completly comfortable with these commands. For instance, create two commits locally and push them on the remote in one go.

Experiencing git diff
---------------------

1. Make a change in the `list.txt` file with your favourite text editor
2. Have a look at the differences with `git diff`
3. Add now these modifications to the staging area and execute again `git diff`, what happens ?
4. Have a try with `git diff --staged`
5. Remove the modifications from the staging area with `git restore --staged list.txt`

Exploring the history of the project
------------------------------------

Locally
1. Look at the history of the local repository with `git log --oneline --graph`
2. Look at differences between different versions
  - `git diff final_shopping_list`
  - `git diff final_shopping_list 0fb6200`

Creating and merging branches
-----------------------------

Try to redo what is done in this [video](https://www.youtube.com/watch?v=FrIK9wY-r3E&list=PLqmwiCS_fyTUVFmfMCOTxGBRP7cRiPQAK&index=9) which is the same as the commands shown on this [picture](https://git.univ-pau.fr/mhaefele/shopping-list/-/blob/91af5635da5badcafb3e1cae42a882dd49243131/branch_internal.pdf). Do it several times until you do not need the picture or the video as support. Do not forget to look at the status and history of the local repository regularly. 

Interacting with remote repositories
------------------------------------

Try to redo what is done in this [video](https://www.youtube.com/watch?v=COrRAvQ576k&list=PLqmwiCS_fyTUVFmfMCOTxGBRP7cRiPQAK&index=13) which is the same as the commands shown on this [picture](https://git.univ-pau.fr/mhaefele/shopping-list/-/blob/91af5635da5badcafb3e1cae42a882dd49243131/remotes.pdf). Do it several times until you do not need the picture or the video as support. Do not forget to look at the status and history of the local and remote repositories regularly. 


Working on the same branch
--------------------------

If you are several following this course, make groups of students of 2-3: Maria, Sebastian and Fatima. You have to choose to use the project of one of you, for instance Maria's project.
So Maria adds Sebastian and Fatima as member of her project with developer rights. Then Sebastian and Fatima clone this project. You are now collaborating on the same project.

1. Fast forward merges
On the master branch, for each group of students:
  * student1 modifies list.txt, commits and pushes
  ```
  #Edit list.txt
  git commit 
  git push origin master
  ```
  * student2 pulls, modifies list.txt, commits and pushes
  ```
  git pull origin master
  #Edit list.txt
  git commit 
  git push origin master
  ```
  * student3 pulls, modifies list.txt, commits and pushes
  ```
  git pull origin master
  #Edit list.txt
  git commit 
  git push origin master
  ```

2. Merges
On the master branch, for each group of students:
  * everybody modifies list.txt, but a **different line**
  * everybody tries to commit & push
  
3. Conflict
On the master branch, for each group of students:
  * student1 and student2 modify **the same line** in list.txt 
  * everybody tries to commit & push
  
Feature branch & merge request
------------------------------
1. Protected branch
  * On the web interface, modify the access property of the master branch to *protected* status (settings/Repository/protected branches)
  * Student1 creates a new commit on the master branch
  * Try to push this new commit on the gtilab server on the master branch.
  * What happens ?

2. Feature branch
  * Student1 creates a new branch (`git branch branch_name`) such that the latest commit belongs to the new branch and not anymore to the master branch
  * Student1 pushes his new branch on the server (`git push origin branch_name`)
  * Student2 creates a new branch from master directly (`git checkout -b branch_name2`)
  * Student2 creates a new commit on this new branch and pushes the branch on the server

3. Merge request
  * On the web interface each student creates a merge request to merge their feature branch to the master one
  * Accept it and verify it gives the correct result

Setting and fixing issues
-------------------------
1. Each student creates a new feature branch like before and pushes it on the server

2. Reporting an issue
  * On the web interface, report an issue on the branch of your colleague
  * Discuss on the issue using the web interface

3. Fixing the issue
  * Each student fixes the issue in his own branch
  * Change the status of the issue by providing the appropriate commit message by mentioning `Fix #2` to close issue number 2 for example. Please refer to [gitlab documentation](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html) for all possible ways to open and close issues.

Exploring the history
---------------------
1. Try to answer the following questions 
  * Who touched this line from this file ?
  * Who modified this part of the repository during a given period ?
  * To which branch does this commit belong ?
  * ...

2. If you managed to answer some questions, show the others how you managed to do it

Stashing modifications
----------------------
1. Stash modifications
  * Modify the local copy of a branch
  * You cannot checkout another branch right now. You have to *save* your modification without creating a proper commit, i.e. to *stash* them with `git stash`.
  * Checkout the other branch
  * Modify it and commit

2. Unstash your modifications
  * Checkout the inital branch again
  * unstash your modfications with `git stash pop`
  * You recover the state you were before
 
Multiple remote repositories
---------------------
1. Add the project of another group of students as a new remote of your local repository
2. Try to merge something and see what happens...

